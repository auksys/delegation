simulation-tst
==============

This crate provides an implementation of a simulator of Task-Specification Trees. This can be used to compute an approximation of the execution of TST. This is useful to estimate the feasibility and the cost of execution.
