pub(crate) fn distance<T: num_traits::Float>(x1: T, y1: T, x2: T, y2: T) -> T
{
  return ((x1 - x2).powi(2) + (y1 - y2).powi(2)).sqrt();
}

pub(crate) fn haversine_distance(
  longitude_a: f64,
  latitude_a: f64,
  longitude_b: f64,
  latitude_b: f64,
) -> f64
{
  let mut r = 6371.0 * 1e3;

  let d_lat: f64 = (latitude_b - latitude_a).to_radians();
  let d_lon: f64 = (longitude_b - longitude_a).to_radians();
  let lat1: f64 = (latitude_a).to_radians();
  let lat2: f64 = (latitude_b).to_radians();

  let a = ((d_lat / 2.0).sin()) * ((d_lat / 2.0).sin())
    + ((d_lon / 2.0).sin()) * ((d_lon / 2.0).sin()) * (lat1.cos()) * (lat2.cos());
  let c = 2.0 * ((a.sqrt()).atan2((1.0 - a).sqrt()));

  return r * c;
}
