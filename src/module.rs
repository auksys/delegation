//! This module contains the definition for an agent_tk module.

use crate::delegation::{InputMessage, OutputMessage};

/// Define an agent module. Each module can receive message as inputs and send via its outputs. All the messages are broadcasted. Each module has a single thread used for handling the messages. If longer computation are needed, they can spawn other modules.
pub trait Module
{
  /// Type of the input message for this module
  type InputMessage;
  /// Type of the output message for this module
  type OutputMessage;
  /// Type of the private interface
  type ModulePrivateInterface: ModulePrivateInterface<Self::InputMessage, Self::OutputMessage>;

  /// This function is called ahead
  fn prepare_interfaces(
    cap: usize,
  ) -> (
    ModuleInterface<Self::InputMessage, Self::OutputMessage>,
    Self::ModulePrivateInterface,
  )
  {
    let (input_sender, mut input_receiver) = async_broadcast::broadcast::<Self::InputMessage>(cap);
    let (output_sender, mut output_receiver) =
      async_broadcast::broadcast::<Self::OutputMessage>(cap);

    (
      ModuleInterface::<Self::InputMessage, Self::OutputMessage>::new(
        input_sender,
        output_receiver.deactivate(),
      ),
      ModulePrivateInterface::new(input_receiver.deactivate(), output_sender),
    )
  }
}

/// Private interface for modules. Created outside of the module, but passed in constructor.
pub trait ModulePrivateInterface<InputMessage, OutputMessage>
{
  /// Create a new private interface
  fn new(
    input_receiver: async_broadcast::InactiveReceiver<InputMessage>,
    output_sender: async_broadcast::Sender<OutputMessage>,
  ) -> Self;
}

/// Public module interface
#[derive(Clone)]
pub struct ModuleInterface<InputMessage, OutputMessage>
{
  input_sender: async_broadcast::Sender<InputMessage>,
  output_receiver: async_broadcast::InactiveReceiver<OutputMessage>,
}

impl<InputMessage, OutputMessage> ModuleInterface<InputMessage, OutputMessage>
{
  fn new(
    input_sender: async_broadcast::Sender<InputMessage>,
    output_receiver: async_broadcast::InactiveReceiver<OutputMessage>,
  ) -> Self
  {
    Self {
      output_receiver,
      input_sender,
    }
  }

  pub(crate) fn output_receiver(&self) -> async_broadcast::Receiver<OutputMessage>
  {
    self.output_receiver.activate_cloned()
  }
  pub(crate) fn input_sender(&self) -> async_broadcast::Sender<InputMessage>
  {
    self.input_sender.clone()
  }
}

macro_rules! create_module_private_interface {
  ($name:ident, $input_message:ty, $output_message:ty) => {
    #[allow(missing_docs)]
    pub struct $name
    {
      input_receiver: async_broadcast::InactiveReceiver<$input_message>,
      output_sender: async_broadcast::Sender<$output_message>,
    }
    impl $crate::module::ModulePrivateInterface<$input_message, $output_message> for $name
    {
      fn new(
        input_receiver: async_broadcast::InactiveReceiver<$input_message>,
        output_sender: async_broadcast::Sender<$output_message>,
      ) -> Self
      {
        Self {
          input_receiver,
          output_sender,
        }
      }
    }
  };
}

pub(crate) use create_module_private_interface;
