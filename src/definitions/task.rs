//! Handle task representations of a task

use crate::definitions::goal as definitions_goal;
use crate::definitions::tst as definitions_tst;
use crate::{delegation, uuid, Error, Result};
use definitions_tst::NodeTrait;
use serde::{Deserialize, Serialize};

//  _____         _     ____            _        _
// |_   _|_ _ ___| | __/ ___|___  _ __ | |_ __ _(_)_ __   ___ _ __
//   | |/ _` / __| |/ / |   / _ \| '_ \| __/ _` | | '_ \ / _ \ '__|
//   | | (_| \__ \   <| |__| (_) | | | | || (_| | | | | |  __/ |
//   |_|\__,_|___/_|\_\\____\___/|_| |_|\__\__,_|_|_| |_|\___|_|
//

/// Contains the different definition of a task
#[derive(Serialize, Deserialize, Clone)]
pub enum TaskContainer
{
  /// A TST-based task definition
  Tst(definitions_tst::Node),
  /// A goal-based task definition
  Goal(definitions_goal::Goal),
}

//  _______        _
// |__   __|      | |
//    | | __ _ ___| | __
//    | |/ _` / __| |/ /
//    | | (_| \__ \   <
//    |_|\__,_|___/_|\_\

/// Represent a task
#[derive(Serialize, Deserialize, Clone)]
pub struct Task
{
  container: TaskContainer,
}

impl Task
{
  /// Create a new task from a TST definition
  pub fn from_tst(node: definitions_tst::Node) -> Task
  {
    Task {
      container: TaskContainer::Tst(node),
    }
  }
  /// Create a new task from a Goal definition
  pub fn from_goal(goal: definitions_goal::Goal) -> Task
  {
    Task {
      container: TaskContainer::Goal(goal),
    }
  }
  /// Return the container
  pub fn get_container(&self) -> &TaskContainer
  {
    &self.container
  }
}

impl delegation::Task for Task
{
  fn from_description(task_type: &String, description: &String) -> Result<Self>
  {
    match task_type.as_str()
    {
      crate::consts::TST => Ok(Task::from_tst(
        definitions_tst::Node::from_json_string(&description).unwrap(),
      )),
      crate::consts::GOAL => Ok(Task::from_goal(
        definitions_goal::Goal::from_json_string(&description).unwrap(),
      )),
      unknown_task_type => Err(Error::UnknownTaskType(unknown_task_type.to_string())),
    }
  }
  fn task_id(&self) -> uuid::Uuid
  {
    match &self.container
    {
      TaskContainer::Tst(tst) => tst.common_params_ref().node_uuid,
      TaskContainer::Goal(goal) => goal.uuid,
    }
  }
  fn task_type(&self) -> &str
  {
    match self.container
    {
      TaskContainer::Tst(_) => crate::consts::TST,
      TaskContainer::Goal(_) => crate::consts::GOAL,
    }
  }
  fn to_description(&self) -> String
  {
    match &self.container
    {
      TaskContainer::Tst(task) => task.to_json_string().unwrap(),
      TaskContainer::Goal(goal) => goal.to_json_string().unwrap(),
    }
  }
}
