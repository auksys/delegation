//! This module allows to represent goals.

use crate::uuid;
use serde::{Deserialize, Serialize};
use std::{collections::BTreeMap, default};

use crate::definitions::misc_structures::GeoPoint;

/// Represent a delivery goal
#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub struct DeliveryGoal {}

/// Represent a visitation goal
#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub struct VisitGoal
{
  /// Position of the goal to visit
  #[serde(flatten)]
  pub position: GeoPoint,
}

/// Represent a set of goals
#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub struct Goal
{
  /// Unique identifier for the goal
  pub uuid: uuid::Uuid,
  #[serde(default = "Default::default")]
  /// Delivery goals
  pub deliver: BTreeMap<String, DeliveryGoal>,
  /// Visit goals
  #[serde(default = "Default::default")]
  pub visit: BTreeMap<String, VisitGoal>,
}

impl Goal
{
  /// Convenient function to convert a node to a JSON string
  pub fn to_json_string(&self) -> serde_json::Result<String>
  {
    serde_json::to_string(self)
  }
  /// Convenient function to convert a JSON string to a node
  pub fn from_json_string(def: &String) -> serde_json::Result<Goal>
  {
    serde_json::from_str(def)
  }
}

#[cfg(test)]
mod tests
{
  use super::*;
  fn mgp(longitude: f64, latitude: f64, altitude: f64) -> GeoPoint
  {
    GeoPoint {
      longitude: longitude,
      latitude: latitude,
      altitude: altitude,
    }
  }
  #[test]
  fn parse_test_goals()
  {
    let mut d = std::path::PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    d.push("data/test_goal.json");
    let message: String = std::fs::read_to_string(d).unwrap();
    let goal: super::Goal = serde_json::from_str(&message).unwrap();

    assert!(goal.deliver.is_empty());
    assert_eq!(goal.visit.len(), 2);
    assert_eq!(
      goal.visit.get("loc1").unwrap().position,
      mgp(16.679423811351608, 57.75966551794022, 0.0)
    );
    assert_eq!(
      goal.visit.get("loc2").unwrap().position,
      mgp(16.677721987885022, 57.75937792909685, 0.0)
    );
  }
}
