//! misc structure for defining TST and goals

use serde::{Deserialize, Serialize};

//  __  __ _            ____  _                   _
// |  \/  (_)___  ___  / ___|| |_ _ __ _   _  ___| |_ _   _ _ __ ___  ___
// | |\/| | / __|/ __| \___ \| __| '__| | | |/ __| __| | | | '__/ _ \/ __|
// | |  | | \__ \ (__   ___) | |_| |  | |_| | (__| |_| |_| | | |  __/\__ \
// |_|  |_|_|___/\___| |____/ \__|_|   \__,_|\___|\__|\__,_|_|  \___||___/

fn zero_f64() -> f64
{
  0.0
}

/// Represent a geographic point in the world
#[derive(Serialize, Deserialize, Debug, Default, PartialEq, Clone)]
pub struct GeoPoint
{
  /// longitude of the point
  pub longitude: f64,
  /// latitude of the point
  pub latitude: f64,
  /// altitude of the point
  #[serde(default = "zero_f64")]
  pub altitude: f64,
}

/// Represent a speed in a tst, some of the values are platform specific
#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub enum Speed
{
  /// A fast speed
  #[serde(rename = "fast")]
  Fast,
  /// The standard speed for the platform, a balance between speed and fuel efficiency
  #[serde(rename = "standard")]
  #[default]
  Standard,
  /// A slow speed
  #[serde(rename = "slow")]
  Slow,
  /// A maximum speed specified as m/s
  #[serde(rename = "max-speed")]
  MaxSpeed(f32),
}
