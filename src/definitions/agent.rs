//! agent
//! =====
//!
//! Support for defining parts of an agent.

pub mod capabilities;
pub mod resources;
