//! Definitions of tsts, for de/serialization and manipulation.

use crate::uuid;
use enum_dispatch::enum_dispatch;
use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;
use std::default;

mod consts;

pub use crate::definitions::misc_structures::{GeoPoint, Speed};

//  _   _           _
// | \ | | ___   __| | ___
// |  \| |/ _ \ / _` |/ _ \
// | |\  | (_) | (_| |  __/
// |_| \_|\___/ \__,_|\___|

/// Represent a TST node
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "name")]
#[enum_dispatch(NodeTrait)]
pub enum Node
{
  /// Noop node
  #[serde(rename = "noop")]
  Noop(Noop),
  /// Sequential node
  #[serde(rename = "seq")]
  Seq(Seq),
  /// Concurrent node
  #[serde(rename = "conc")]
  Conc(Conc),
  /// MoveTo node
  #[serde(rename = "move-to")]
  MoveTo(MoveTo),
  /// SearchArea node
  #[serde(rename = "search-area")]
  SearchArea(SearchArea),
}

impl Node
{
  /// Convenient function to convert a node to a JSON string
  pub fn to_json_string(&self) -> serde_json::Result<String>
  {
    serde_json::to_string(self)
  }
  /// Convenient function to convert a JSON string to a node
  pub fn from_json_string(def: &String) -> serde_json::Result<Node>
  {
    serde_json::from_str(def)
  }
}

impl Default for Node
{
  fn default() -> Self
  {
    Self::Noop(Noop::default())
  }
}

//  ____                                _
// |  _ \ __ _ _ __ __ _ _ __ ___   ___| |_ ___ _ __ ___
// | |_) / _` | '__/ _` | '_ ` _ \ / _ \ __/ _ \ '__/ __|
// |  __/ (_| | | | (_| | | | | | |  __/ ||  __/ |  \__ \
// |_|   \__,_|_|  \__,_|_| |_| |_|\___|\__\___|_|  |___/

/// Parameters common to all the nodes
#[skip_serializing_none]
#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub struct CommonParameters
{
  /// Unique identifier for the node
  pub node_uuid: uuid::Uuid,
  /// Execution unit
  pub execunit: Option<String>,
}

/// Parameters for the move to tst
#[skip_serializing_none]
#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub struct MoveToParameters
{
  /// Destination
  pub waypoint: GeoPoint,
  /// Optional speed
  pub speed: Option<Speed>,
}

/// Parmeters for the search area test
#[skip_serializing_none]
#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub struct SearchAreaParameters
{
  /// Polygon to explore
  pub area: Vec<GeoPoint>,
  /// Optional speed
  pub speed: Option<Speed>,
}

/// Use for nodes without parameters
#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub struct NoParameters {}

//  _   _           _      ____        _ _     _
// | \ | | ___   __| | ___| __ ) _   _(_) | __| | ___ _ __ ___
// |  \| |/ _ \ / _` |/ _ \  _ \| | | | | |/ _` |/ _ \ '__/ __|
// | |\  | (_) | (_| |  __/ |_) | |_| | | | (_| |  __/ |  \__ \
// |_| \_|\___/ \__,_|\___|____/ \__,_|_|_|\__,_|\___|_|  |___/

/// This class is used to build tsts
#[allow(private_bounds)]
#[derive(Debug, Clone)]
pub struct NodeBuilder<T: CompositeNode, P: NodeBuilderTrait>
{
  t: std::rc::Rc<std::cell::RefCell<T>>,
  p: Option<P>,
}

/// Root null builder
#[derive(Clone)]
pub struct NullNodeBuilder;

//  _____          _ _
// |_   _| __ __ _(_) |_ ___
//   | || '__/ _` | | __/ __|
//   | || | | (_| | | |_\__ \
//   |_||_|  \__,_|_|\__|___/

trait NodeBuilderTrait: Clone
{
  fn append_child(&mut self, node: impl Into<Node>);
}

/// Base trait for nodes
#[enum_dispatch]
pub trait NodeTrait: Clone + Into<Node> + Default
{
  /// Return a reference to the common params of a node
  fn common_params_ref(&self) -> &CommonParameters;
  /// Set the comment parameter for a node
  fn set_common_params(&mut self, params: CommonParameters);
}

/// Composite node, aka, node with children such as Conc and Seq
#[allow(private_bounds)]
pub trait CompositeNode: NodeTrait + IntoIterator + CompositeNodeBuildable
{
  /// Start building a tree
  fn build(common_params: Option<CommonParameters>) -> NodeBuilder<Self, NullNodeBuilder>;

  /// Returns an iterator over the children of the composite node.
  ///
  /// The iterator yields all items from start to end.
  fn iter(&self) -> core::slice::Iter<'_, Node>;
}

trait CompositeNodeBuildable
{
  /// Add a child node
  fn append_child(&mut self, node: impl Into<Node>);
}

trait LeafNode: NodeTrait
{
  type ParametersType: Clone + Default;
  fn set_params(&mut self, params: Self::ParametersType);
}

trait LeafNodeParameters
{
  type NodeType: Default + LeafNode + Into<Node>;
}

// Nodes

macro_rules! define_composite_node {
  ($node_type:ident, $node_enum_type:path) => {
    /// Node for $node_type
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct $node_type
    {
      /// Common parameters
      pub common_params: CommonParameters,
      /// Childrent of the composite node
      pub children: Vec<Node>,
    }
    impl $node_type
    {
      #[allow(private_interfaces)]
      /// Start building a node
      pub fn build(
        common_params: Option<CommonParameters>,
      ) -> NodeBuilder<$node_type, NullNodeBuilder>
      {
        Self::build_(common_params)
      }
      #[allow(private_interfaces)]
      fn build_(common_params: Option<CommonParameters>)
        -> NodeBuilder<$node_type, NullNodeBuilder>
      {
        let mut t = $node_type::default();
        if let Some(common_params) = common_params
        {
          t.common_params = common_params;
        }
        NodeBuilder::<$node_type, NullNodeBuilder> {
          t: std::rc::Rc::new(std::cell::RefCell::new(t)),
          p: None,
        }
      }
    }
    impl NodeTrait for $node_type
    {
      fn common_params_ref(&self) -> &CommonParameters
      {
        &self.common_params
      }
      fn set_common_params(&mut self, common_params: CommonParameters)
      {
        self.common_params = common_params;
      }
    }
    impl CompositeNodeBuildable for $node_type
    {
      fn append_child(&mut self, node: impl Into<Node>)
      {
        self.children.push(node.into());
      }
    }
    impl CompositeNode for $node_type
    {
      #[allow(private_interfaces)]
      fn build(common_params: Option<CommonParameters>)
        -> NodeBuilder<$node_type, NullNodeBuilder>
      {
        Self::build_(common_params)
      }
      fn iter(&self) -> core::slice::Iter<'_, Node>
      {
        self.children.iter()
      }
    }

    impl IntoIterator for $node_type
    {
      type Item = Node;
      type IntoIter = std::vec::IntoIter<Node>;
      fn into_iter(self) -> Self::IntoIter
      {
        self.children.into_iter()
      }
    }
  };
}

macro_rules! define_leaf_node {
  ($node_type:ident, $node_parameters_type:ident, $node_enum_type:path) => {
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    /// Node for $node_type
    pub struct $node_type
    {
      /// Common parameters
      pub common_params: CommonParameters,
      /// Parameters specific to the node
      pub params: $node_parameters_type,
    }
    impl NodeTrait for $node_type
    {
      fn common_params_ref(&self) -> &CommonParameters
      {
        &self.common_params
      }
      fn set_common_params(&mut self, common_params: CommonParameters)
      {
        self.common_params = common_params;
      }
    }
    impl LeafNode for $node_type
    {
      type ParametersType = $node_parameters_type;
      fn set_params(&mut self, params: Self::ParametersType)
      {
        self.params = params;
      }
    }
    impl LeafNodeParameters for $node_parameters_type
    {
      type NodeType = $node_type;
    }
  };
}

define_composite_node!(Seq, Node::Seq);
define_composite_node!(Conc, Node::Conc);

define_leaf_node!(MoveTo, MoveToParameters, Node::MoveTo);
define_leaf_node!(SearchArea, SearchAreaParameters, Node::SearchArea);
define_leaf_node!(Noop, NoParameters, Node::Noop);

// Implementation

/// Trait used to compute the velocity
pub trait SpeedCompute
{
  /// Compute the actual velocity based on the maximum velocity of the platform
  fn compute_velocity(&self, max_velocity: f32) -> f32;
}

impl SpeedCompute for Option<Speed>
{
  fn compute_velocity(&self, max_velocity: f32) -> f32
  {
    match self
    {
      Some(v) => match v
      {
        Speed::Slow => consts::VELOCITY_SLOW_RATIO * max_velocity,
        Speed::Standard => consts::VELOCITY_STANDARD_RATIO * max_velocity,
        Speed::Fast => consts::VELOCITY_FAST_RATIO * max_velocity,
        Speed::MaxSpeed(s) => s.min(max_velocity),
      },
      None => consts::VELOCITY_STANDARD_RATIO * max_velocity,
    }
  }
}

#[allow(missing_docs)]
#[inline]
pub fn default<T: Default>() -> T
{
  Default::default()
}

impl NodeBuilderTrait for NullNodeBuilder
{
  fn append_child(&mut self, _node: impl Into<Node>)
  {
    panic!("Cannot add child to null builder.");
  }
}

impl<T: CompositeNode, P: NodeBuilderTrait> NodeBuilderTrait for NodeBuilder<T, P>
{
  fn append_child(&mut self, node: impl Into<Node>)
  {
    self.t.borrow_mut().append_child(node);
  }
}

#[allow(private_bounds)]
impl<T: CompositeNode, P: NodeBuilderTrait> NodeBuilder<T, P>
{
  /// Start a composite node, must be followed by end
  pub fn start<U: CompositeNode>(
    self,
    cp: Option<CommonParameters>,
  ) -> NodeBuilder<U, NodeBuilder<T, P>>
  {
    let mut t = U::default();
    if let Some(cp) = cp
    {
      t.set_common_params(cp);
    }
    NodeBuilder::<U, NodeBuilder<T, P>> {
      t: std::rc::Rc::new(std::cell::RefCell::new(t)),
      p: Some(self),
    }
  }
  /// end a composite node
  pub fn end(self) -> P
  {
    let mut p = self.p.unwrap();
    p.append_child(self.t.borrow().to_owned());
    p
  }
  /// Add a leaf node
  #[allow(private_interfaces)]
  pub fn add<U>(self, p: U, cp: Option<CommonParameters>) -> Self
  where
    U: LeafNodeParameters + Into<<<U as LeafNodeParameters>::NodeType as LeafNode>::ParametersType>,
  {
    let mut t = U::NodeType::default();
    t.set_params(p.into());
    if let Some(cp) = cp
    {
      t.set_common_params(cp);
    }
    self.t.borrow_mut().append_child(t);
    self
  }
  /// Add to the node
  pub fn add_node(self, n: Node) -> Self
  {
    self.t.borrow_mut().append_child(n);
    self
  }
  /// Convert to the type
  fn to_t(&self) -> T
  {
    self.t.borrow().clone()
  }
}

impl<T: CompositeNode, U: NodeBuilderTrait> From<NodeBuilder<T, U>> for Node
where
  T: Into<Node>,
{
  fn from(value: NodeBuilder<T, U>) -> Self
  {
    value.to_t().into()
  }
}

// Tests

#[cfg(test)]
mod tests
{
  use crate::definitions::tst::CommonParameters;

  use super::{CompositeNode, GeoPoint};

  macro_rules! get_tst_node {
    ($value:expr, $variant:path) => {
      match $value
      {
        $variant(x) => x,
        _ => panic!("Unexpected TST Node."),
      }
    };
  }

  macro_rules! check_speed {
    ($value:expr, $variant:path) => {
      match $value.params.speed.as_ref().unwrap()
      {
        $variant => (),
        _ => panic!("Wrong speed"),
      }
    };
  }

  macro_rules! check_max_speed {
    ($value:expr, $max_speed:expr) => {
      match $value.params.speed.as_ref().unwrap()
      {
        super::Speed::MaxSpeed(v) => assert_eq!(*v, $max_speed),
        _ => panic!("Wrong speed"),
      }
    };
  }

  fn mgp(longitude: f64, latitude: f64, altitude: f64) -> GeoPoint
  {
    GeoPoint {
      longitude: longitude,
      latitude: latitude,
      altitude: altitude,
    }
  }
  #[test]
  fn parse_test_tst()
  {
    let mut d = std::path::PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    d.push("data/test_tst.json");
    let message: String = std::fs::read_to_string(d).unwrap();
    let node: super::Node = serde_json::from_str(&message).unwrap();
    let node_seq = get_tst_node!(node, super::Node::Seq);

    // First move
    let seq_move_to_0 = get_tst_node!(&node_seq.children[0], super::Node::MoveTo);
    assert_eq!(
      seq_move_to_0.params.waypoint,
      mgp(15.570883999999998, 58.394741999999994, 100.8)
    );
    check_speed!(seq_move_to_0, super::Speed::Standard);

    // Concurrent
    let seq_conc = get_tst_node!(&node_seq.children[1], super::Node::Conc);
    let conc_searc_area = get_tst_node!(&seq_conc.children[0], super::Node::SearchArea);
    assert_eq!(
      conc_searc_area.params.area[0],
      mgp(15.570883999999998, 58.394741999999994, 100.8)
    );
    assert_eq!(
      conc_searc_area.params.area[1],
      mgp(15.570883999999998, 58.30474199999999, 100.8)
    );
    assert_eq!(
      conc_searc_area.params.area[2],
      mgp(15.500883999999998, 58.30474199999999, 100.8)
    );
    check_max_speed!(conc_searc_area, 10.0);

    let conc_move_to = get_tst_node!(&seq_conc.children[1], super::Node::MoveTo);
    assert_eq!(
      conc_move_to.params.waypoint,
      mgp(15.571883999999998, 58.39474299999999, 100.8)
    );
    assert!(conc_move_to.params.speed.is_none());

    // Second move
    let seq_move_to_2 = get_tst_node!(&node_seq.children[2], super::Node::MoveTo);
    assert_eq!(
      seq_move_to_2.params.waypoint,
      mgp(16.572883999999998, 59.39474169999999, 110.8)
    );
    check_speed!(seq_move_to_2, super::Speed::Fast);
  }
  #[test]
  fn gen_tst()
  {
    let node = super::Node::Seq(super::Seq {
      common_params: CommonParameters {
        node_uuid: crate::uuid::Uuid::from_string("f3bab5e0-a861-4f6e-bf28-37c9e3470181").unwrap(),
        ..Default::default()
      },
      ..Default::default()
    });
    assert_eq!(
      serde_json::to_string(&node).unwrap(),
      r#"{"name":"seq","common_params":{"node_uuid":"f3bab5e0-a861-4f6e-bf28-37c9e3470181"},"children":[]}"#
    );
  }
  #[test]
  fn test_builder()
  {
    let node: super::Node = super::Conc::build(Some(CommonParameters {
      node_uuid: crate::uuid::Uuid::from_string("68eb7e83-cc44-4404-bdb1-2fd66ffcbf1c").unwrap(),
      ..Default::default()
    }))
    .start::<super::Seq>(Some(CommonParameters {
      node_uuid: crate::uuid::Uuid::from_string("a5665c27-e2c6-442f-8ab3-357aa4427945").unwrap(),
      ..Default::default()
    }))
    .add(
      super::MoveToParameters {
        waypoint: GeoPoint {
          longitude: 16.4,
          latitude: 59.3,
          altitude: 110.8,
        },

        ..super::default()
      },
      Some(CommonParameters {
        node_uuid: crate::uuid::Uuid::from_string("0cd50294-611a-4da6-b398-73b0f2e0ee7c").unwrap(),
        ..Default::default()
      }),
    )
    .add(
      super::MoveToParameters {
        waypoint: GeoPoint {
          longitude: 16.5,
          latitude: 59.5,
          altitude: 110.7,
        },
        ..super::default()
      },
      Some(CommonParameters {
        node_uuid: crate::uuid::Uuid::from_string("47288783-cfbd-48ad-9bb9-734fae54d2be").unwrap(),
        ..Default::default()
      }),
    )
    .end()
    .add(
      super::MoveToParameters {
        waypoint: GeoPoint {
          longitude: 16.6,
          latitude: 59.4,
          altitude: 110.9,
        },
        ..super::default()
      },
      Some(CommonParameters {
        node_uuid: crate::uuid::Uuid::from_string("88eae25f-473f-4185-bfcb-ca3ee0ed2247").unwrap(),
        ..Default::default()
      }),
    )
    .into();
    assert_eq!(
      serde_json::to_string(&node).unwrap(),
      r#"{"name":"conc","common_params":{"node_uuid":"68eb7e83-cc44-4404-bdb1-2fd66ffcbf1c"},"children":[{"name":"seq","common_params":{"node_uuid":"a5665c27-e2c6-442f-8ab3-357aa4427945"},"children":[{"name":"move-to","common_params":{"node_uuid":"0cd50294-611a-4da6-b398-73b0f2e0ee7c"},"params":{"waypoint":{"longitude":16.4,"latitude":59.3,"altitude":110.8}}},{"name":"move-to","common_params":{"node_uuid":"47288783-cfbd-48ad-9bb9-734fae54d2be"},"params":{"waypoint":{"longitude":16.5,"latitude":59.5,"altitude":110.7}}}]},{"name":"move-to","common_params":{"node_uuid":"88eae25f-473f-4185-bfcb-ca3ee0ed2247"},"params":{"waypoint":{"longitude":16.6,"latitude":59.4,"altitude":110.9}}}]}"#
    );
  }
}
