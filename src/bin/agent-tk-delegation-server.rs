use agent_tk::delegation::Task;
use futures::FutureExt;
use std::time::Duration;
use yaaral::RuntimeInterface;

mod consts
{
  pub const MQTT_NODE_ID: &str = "org.auksys.agent-tk.delegation-server";
  pub const MQTT_HOSTNAME: &str = "localhost";
  pub const MQTT_PORT: u16 = 1883;
}

#[derive(serde::Deserialize)]
struct StartDelegationRequest
{
  task_type: String,
  task_description: String,
}

#[derive(serde::Serialize)]
struct StartDelegationAnswer
{
  success: bool,
  message: String,
}

struct TaskExecutor {}

impl agent_tk::execution::default::TaskExecutor for TaskExecutor
{
  fn execute_task(
    &self,
    _: agent_tk::definitions::task::Task,
  ) -> futures::future::BoxFuture<'static, agent_tk::Result<()>>
  {
    async { Ok(()) }.boxed()
  }
  fn can_execute(&self, _: &agent_tk::definitions::task::Task) -> bool
  {
    false
  }
}

fn main()
{
  let runtime = yaaral::Runtime::new("agent-tk-delegation-server").expect("a runtime");
  // rt.spawn_task(async {})
  let mut mqtt_connection = mqtt_service::Connection::new(
    format!("{}-services", consts::MQTT_NODE_ID),
    consts::MQTT_HOSTNAME,
    consts::MQTT_PORT,
  );

  let agent = agent_tk::Agent::new::<
    agent_tk::delegation::mqtt::Module,
    agent_tk::decision::default::Module,
    agent_tk::execution::default::Module,
  >(
    agent_tk::agent::AgentData {
      agent_uri: "agent-tk-delegation-server".to_string(),
      async_runtime: runtime.to_owned(),
      knowledge_base: Box::new(agent_tk::knowledge_base::InMemoryBase::new()),
      states: agent_tk::states::States::default().into(),
      capabilities: agent_tk::definitions::agent::capabilities::Capabilities::default(),
      projection: agent_tk::projection::Projection::new(0.0, 0.0),
    },
    agent_tk::delegation::mqtt::Options::new(
      format!("{}_agent_delegation", crate::consts::MQTT_NODE_ID),
      crate::consts::MQTT_HOSTNAME.to_owned(),
      crate::consts::MQTT_PORT,
    ),
    agent_tk::decision::default::Options::new(),
    agent_tk::execution::default::Options::new(TaskExecutor {}),
  )
  .expect("to have successfully created an agent");

  mqtt_connection
    .create_json_service(
      "delegation_server/delegate",
      Box::new(move |request: &StartDelegationRequest| {
        let f = || {
          let task = agent_tk::definitions::task::Task::from_description(
            &request.task_type,
            &request.task_description,
          )?;
          let fut = agent.delegate_task(task);
          let result = runtime.block_on(runtime.spawn_task(fut).expect("the task to be spawned"));
          let result = match result
          {
            Ok(_) => StartDelegationAnswer {
              success: true,
              message: Default::default(),
            },
            Err(e) => StartDelegationAnswer {
              success: false,
              message: e.to_string(),
            },
          };
          Ok::<StartDelegationAnswer, agent_tk::Error>(result)
        };
        match f()
        {
          Ok(r) => r,
          Err(e) => StartDelegationAnswer {
            success: false,
            message: format!("Failed to call service: {}", e.to_string()),
          },
        }
      }),
    )
    .expect("service to be added");

  loop
  {
    std::thread::sleep(Duration::from_secs(60 * 60 * 24));
  }
}
