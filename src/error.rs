/// Error enum for agent_tk
#[derive(thiserror::Error, Debug)]
#[allow(missing_docs)]
pub enum Error
{
  #[error("UTF8 Error: '{0}'")]
  Utf8Error(#[from] std::str::Utf8Error),
  #[error("MQTT Connection Error: '{0}'")]
  #[cfg(feature = "mqtt")]
  MQTTConnectionError(#[from] mqtt_channel::Error),
  #[error("MQTT Service Error: '{0}'")]
  #[cfg(feature = "mqtt")]
  MQTTServiceError(#[from] mqtt_service::Error),
  #[error("TimeoutError: {0}")]
  TimeoutError(#[from] async_std::future::TimeoutError),
  #[error("Capability '{0}' is unknown.")]
  UnknownCapability(&'static str),
  #[error("Capability '{0}' already defined.")]
  DuplicateCapability(&'static str),
  #[error("An error occured during transport: '{0}'.")]
  TransportError(String),
  #[error("Invalid task type: {0}.")]
  InvalidTaskType(String),
  #[error("Unknown task type: {0}.")]
  UnknownTaskType(String),
  #[error("State '{0}' is unknown.")]
  UnknownState(&'static str),
  #[error("State '{0}' already defined.")]
  DuplicateState(&'static str),
  #[error("The execution failed: {0}.")]
  ExecutionFailed(#[from] Box<Error>),
  #[error("The task '{0}' cannot be executed.")]
  CannotExecute(String),
  #[error("No executor.")]
  NoExecutor(),
  #[error("Invalid collection: {0}.")]
  InvalidCollection(String),
  #[error("Unknown value in collection {0} with key {1}.")]
  UnknownValue(String, String),
  #[error("Serialization error: {0}")]
  JsonSerializationError(#[from] serde_json::Error),
  #[error("Serialization error: {0}")]
  CborSerializationError(#[from] ciborium::ser::Error<std::io::Error>),
  #[error("Deserialization error: {0}")]
  DeserializationError(String),
  #[error("Mutex error: {0}")]
  MutexError(String),
  #[error("Runtime error: {0}")]
  Runtime(String),
  #[error("Invalid UUID: {0} with error {1}")]
  InvalidUuid(String, String),
  #[error("Goals cannot be executed.")]
  CannotExecuteGoals,
  #[error("Futures spawn error {0}")]
  FutureSpawnError(#[from] futures::task::SpawnError),
  #[error("Unimplemented: {0}.")]
  Unimplemented(&'static str),
  #[error("InternalError: {0}.")]
  InternalError(&'static str),
}

impl<'a, T> From<std::sync::PoisonError<std::sync::MutexGuard<'a, T>>> for Error
{
  fn from(value: std::sync::PoisonError<std::sync::MutexGuard<'a, T>>) -> Self
  {
    Error::MutexError(value.to_string())
  }
}

impl<'a, T> From<std::sync::PoisonError<std::sync::RwLockWriteGuard<'a, T>>> for Error
{
  fn from(value: std::sync::PoisonError<std::sync::RwLockWriteGuard<'a, T>>) -> Self
  {
    Error::MutexError(value.to_string())
  }
}

impl From<yaaral::NoError> for Error
{
  fn from(value: yaaral::NoError) -> Self
  {
    Error::Unimplemented("yaaral::NoError should never trigger.")
  }
}
