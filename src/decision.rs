//! This module handle the decision for the agent.

use crate::definitions::agent as definitions_agent;
use crate::{agent, delegation, execution, knowledge_base, module, utils, uuid};

pub mod default;

//  ___                   _   __  __
// |_ _|_ __  _ __  _   _| |_|  \/  | ___  ___ ___  __ _  __ _  ___
//  | || '_ \| '_ \| | | | __| |\/| |/ _ \/ __/ __|/ _` |/ _` |/ _ \
//  | || | | | |_) | |_| | |_| |  | |  __/\__ \__ \ (_| | (_| |  __/
// |___|_| |_| .__/ \__,_|\__|_|  |_|\___||___/___/\__,_|\__, |\___|
//           |_|                                         |___/

/// Input messages to the decision module
#[derive(Clone)]
pub enum InputMessage
{
  /// Request to take a decision on a CFP
  DecideCFPAcceptance
  {
    /// CFP
    cfp: delegation::CFP,
  },
  /// Queue the execution of a task
  QueueExecution
  {
    /// uuid of the task
    uuid: uuid::Uuid,
    /// Uri of the requester agent
    requester_uri: String,
  },
  /// Cancel the execution of a task
  CancelExecution
  {
    /// uuid of the task
    uuid: uuid::Uuid,
  },
}

//   ___        _               _   __  __
//  / _ \ _   _| |_ _ __  _   _| |_|  \/  | ___  ___ ___  __ _  __ _  ___
// | | | | | | | __| '_ \| | | | __| |\/| |/ _ \/ __/ __|/ _` |/ _` |/ _ \
// | |_| | |_| | |_| |_) | |_| | |_| |  | |  __/\__ \__ \ (_| | (_| |  __/
//  \___/ \__,_|\__| .__/ \__,_|\__|_|  |_|\___||___/___/\__,_|\__, |\___|
//                 |_|                                         |___/

/// Output messages of the decision module
#[derive(Clone)]
pub enum OutputMessage
{
  /// Proposal for a CFP
  CFPProposal
  {
    /// Proposal for a CFP
    proposal: delegation::Proposal,
  },
  /// Result of queuing the execution of a task
  QueueExecutionResult
  {
    /// uuid of the task
    uuid: uuid::Uuid,
    /// Uri of the requester agent
    requester_uri: String,
    /// True if the execution was accepted and queued
    accepted: bool,
  },
}

//  __  __           _       _      ___       _             __
// |  \/  | ___   __| |_   _| | ___|_ _|_ __ | |_ ___ _ __ / _| __ _  ___ ___
// | |\/| |/ _ \ / _` | | | | |/ _ \| || '_ \| __/ _ \ '__| |_ / _` |/ __/ _ \
// | |  | | (_) | (_| | |_| | |  __/| || | | | ||  __/ |  |  _| (_| | (_|  __/
// |_|  |_|\___/ \__,_|\__,_|_|\___|___|_| |_|\__\___|_|  |_|  \__,_|\___\___|

/// Module interface for the decision module
pub type ModuleInterface = module::ModuleInterface<InputMessage, OutputMessage>;

//  __  __           _       _
// |  \/  | ___   __| |_   _| | ___
// | |\/| |/ _ \ / _` | | | | |/ _ \
// | |  | | (_) | (_| | |_| | |  __/
// |_|  |_|\___/ \__,_|\__,_|_|\___|

/// Trait to define the interface to decision module
pub trait Module:
  module::Module<InputMessage = InputMessage, OutputMessage = OutputMessage> + Sized
{
  /// Options structure used to start the decision module
  type Options;
  /// Create a new delegation module
  fn start(
    agent_data: agent::AgentData,
    module_interfaces: (ModuleInterface, Self::ModulePrivateInterface),
    execution_interface: execution::ModuleInterface,
    options: Self::Options,
  ) -> impl std::future::Future<Output = ()> + std::marker::Send + 'static;
}
