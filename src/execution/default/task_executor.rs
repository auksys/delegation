use futures::FutureExt;

use crate::definitions::task as definitions_task;
use crate::execution::tst::CreatableFromAgent;
use crate::{agent, delegation, execution, module, utils, Error, Result};

//  _____         _    _____                     _
// |_   _|_ _ ___| | _| ____|_  _____  ___ _   _| |_ ___  _ __
//   | |/ _` / __| |/ /  _| \ \/ / _ \/ __| | | | __/ _ \| '__|
//   | | (_| \__ \   <| |___ >  |  __/ (__| |_| | || (_) | |
//   |_|\__,_|___/_|\_\_____/_/\_\___|\___|\__,_|\__\___/|_|

struct TaskExecutor<TTstExecutor>
where
  TTstExecutor: execution::tst::TreeExecutorTrait + Send + Sync + 'static,
{
  tst_executor: TTstExecutor,
}

impl<TTstExecutor> execution::default::TaskExecutor for TaskExecutor<TTstExecutor>
where
  TTstExecutor: execution::tst::TreeExecutorTrait + Send + Sync,
{
  fn can_execute(&self, task: &crate::definitions::task::Task) -> bool
  {
    match task.get_container()
    {
      definitions_task::TaskContainer::Tst(_) => true,
      definitions_task::TaskContainer::Goal(_) => false,
    }
  }
  fn execute_task(
    &self,
    task: crate::definitions::task::Task,
  ) -> futures::future::BoxFuture<'static, Result<()>>
  {
    match task.get_container()
    {
      definitions_task::TaskContainer::Tst(tst) => self.tst_executor.execute(tst.to_owned()),
      definitions_task::TaskContainer::Goal(goal) =>
      {
        return async { Err(Error::CannotExecuteGoals) }.boxed()
      }
    };
    async { Ok(()) }.boxed()
  }
}

impl<TTstExecutor> TaskExecutor<TTstExecutor>
where
  TTstExecutor: execution::tst::TreeExecutorTrait + Send + Sync,
{
  fn new<TAgent>(agent: &TAgent) -> Self
  where
    TTstExecutor::SeqExecutor: CreatableFromAgent<TAgent, TTstExecutor::SeqExecutor>,
    TTstExecutor::ConcExecutor: CreatableFromAgent<TAgent, TTstExecutor::ConcExecutor>,
    TTstExecutor::MoveToExecutor: CreatableFromAgent<TAgent, TTstExecutor::MoveToExecutor>,
    TTstExecutor::SearchAreaExecutor: CreatableFromAgent<TAgent, TTstExecutor::SearchAreaExecutor>,
  {
    Self {
      tst_executor: TTstExecutor::from_agent(agent),
    }
  }
}
