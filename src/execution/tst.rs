//! Support for executing tst.

use futures::future::FutureExt;

use crate::{definitions, utils, Error, Result};

/// Result of executing a TST
pub type ExecutionResult<'a> = futures::future::BoxFuture<'a, Result<()>>;

//  _____                     _
// | ____|_  _____  ___ _   _| |_ ___  _ __ ___
// |  _| \ \/ / _ \/ __| | | | __/ _ \| '__/ __|
// | |___ >  <  __/ (__| |_| | || (_) | |  \__ \
// |_____/_/\_\___|\___|\__,_|\__\___/|_|  |___/

/// Trait representing the executors, used for dispatch
pub trait Executors: Sync
{
  /// Execute a sequential action
  fn execute_seq(&self, tst_node: definitions::tst::Seq) -> ExecutionResult;
  /// Execute a concurrent action
  fn execute_conc(&self, tst_node: definitions::tst::Conc) -> ExecutionResult;
  /// Execute a move to action
  fn execute_move_to(&self, tst_node: definitions::tst::MoveTo) -> ExecutionResult;
  /// Execute a search area action
  fn execute_search_area(&self, tst_node: definitions::tst::SearchArea) -> ExecutionResult;
}

//  _____                     _
// | ____|_  _____  ___ _   _| |_ ___  _ __
// |  _| \ \/ / _ \/ __| | | | __/ _ \| '__|
// | |___ >  <  __/ (__| |_| | || (_) | |
// |_____/_/\_\___|\___|\__,_|\__\___/|_|
//

/// Executor trait
pub trait Executor<T>: Send
{
  /// Execute the node t
  fn execute<'a, 'b>(&'a self, executors: &'b impl Executors, t: T) -> ExecutionResult<'b>;
}

// Dispatch

fn dispatch_execution(
  executors: &impl Executors,
  tst_node: definitions::tst::Node,
) -> ExecutionResult
{
  match tst_node
  {
    definitions::tst::Node::Noop(noop) => async { Ok(()) }.boxed(),
    definitions::tst::Node::Seq(seq) => executors.execute_seq(seq),
    definitions::tst::Node::Conc(conc) => executors.execute_conc(conc),
    definitions::tst::Node::MoveTo(move_to) => executors.execute_move_to(move_to),
    definitions::tst::Node::SearchArea(search_area) => executors.execute_search_area(search_area),
  }
}

//  ____             _____                     _
// / ___|  ___  __ _| ____|_  _____  ___ _   _| |_ ___  _ __
// \___ \ / _ \/ _` |  _| \ \/ / _ \/ __| | | | __/ _ \| '__|
//  ___) |  __/ (_| | |___ >  <  __/ (__| |_| | || (_) | |
// |____/ \___|\__, |_____/_/\_\___|\___|\__,_|\__\___/|_|
//                |_|

/// Sequential Executor
pub struct SeqExecutor;

impl Executor<definitions::tst::Seq> for SeqExecutor
{
  fn execute<'a, 'b>(
    &'a self,
    executors: &'b impl Executors,
    t: definitions::tst::Seq,
  ) -> ExecutionResult<'b>
  {
    async move {
      for sub_task in t.children.iter()
      {
        dispatch_execution(executors, sub_task.to_owned()).await?;
      }
      Ok::<(), Error>(())
    }
    .boxed()
  }
}

//   ____                 _____                     _
//  / ___|___  _ __   ___| ____|_  _____  ___ _   _| |_ ___  _ __
// | |   / _ \| '_ \ / __|  _| \ \/ / _ \/ __| | | | __/ _ \| '__|
// | |__| (_) | | | | (__| |___ >  <  __/ (__| |_| | || (_) | |
//  \____\___/|_| |_|\___|_____/_/\_\___|\___|\__,_|\__\___/|_|
//

/// Concurrent Executor
pub struct ConcExecutor;

impl Executor<definitions::tst::Conc> for ConcExecutor
{
  fn execute<'a, 'b>(
    &'a self,
    executors: &'b impl Executors,
    t: definitions::tst::Conc,
  ) -> ExecutionResult<'b>
  {
    async move {
      let mut futures = vec![];
      for sub_task in t.children.iter()
      {
        futures.push(dispatch_execution(executors, sub_task.to_owned()));
      }
      futures::future::try_join_all(futures).await?;
      Ok::<(), Error>(())
    }
    .boxed()
  }
}

//  _   _       _____                     _
// | \ | | ___ | ____|_  _____  ___ _   _| |_ ___  _ __
// |  \| |/ _ \|  _| \ \/ / _ \/ __| | | | __/ _ \| '__|
// | |\  | (_) | |___ >  <  __/ (__| |_| | || (_) | |
// |_| \_|\___/|_____/_/\_\___|\___|\__,_|\__\___/|_|
//

/// No Executor, it can be used for agent that don't implement some of the executors
pub struct NoExecutor<T: Send>
{
  ghost: std::marker::PhantomData<T>,
}

impl<T: Send> Executor<T> for NoExecutor<T>
{
  fn execute<'a, 'b>(&'a self, _executors: &'b impl Executors, _t: T) -> ExecutionResult<'b>
  {
    async move { Err::<(), Error>(Error::NoExecutor()) }.boxed()
  }
}

//     _                    _
//    / \   __ _  ___ _ __ | |_
//   / _ \ / _` |/ _ \ '_ \| __|
//  / ___ \ (_| |  __/ | | | |_
// /_/   \_\__, |\___|_| |_|\__|
//         |___/

/// Agent trait for TST execution
pub trait Agent {}

//   ____                _        _     _      _____                       _                    _
//  / ___|_ __ ___  __ _| |_ __ _| |__ | | ___|  ___| __ ___  _ __ ___    / \   __ _  ___ _ __ | |_
// | |   | '__/ _ \/ _` | __/ _` | '_ \| |/ _ \ |_ | '__/ _ \| '_ ` _ \  / _ \ / _` |/ _ \ '_ \| __|
// | |___| | |  __/ (_| | || (_| | |_) | |  __/  _|| | | (_) | | | | | |/ ___ \ (_| |  __/ | | | |_
//  \____|_|  \___|\__,_|\__\__,_|_.__/|_|\___|_|  |_|  \___/|_| |_| |_/_/   \_\__, |\___|_| |_|\__|
//                                                                             |___/

/// Trait for creating executor for an agent
pub trait CreatableFromAgent<TAgent, T>
{
  /// Create an executor from an agent
  fn from_agent(agent: &TAgent) -> T;
}

impl<TAgent> CreatableFromAgent<TAgent, SeqExecutor> for SeqExecutor
{
  fn from_agent(_agent: &TAgent) -> SeqExecutor
  {
    SeqExecutor {}
  }
}
impl<TAgent> CreatableFromAgent<TAgent, ConcExecutor> for ConcExecutor
{
  fn from_agent(_agent: &TAgent) -> ConcExecutor
  {
    ConcExecutor {}
  }
}

impl<TAgent, T: Send> CreatableFromAgent<TAgent, NoExecutor<T>> for NoExecutor<T>
{
  fn from_agent(_agent: &TAgent) -> NoExecutor<T>
  {
    NoExecutor {
      ghost: Default::default(),
    }
  }
}

//  _____              _____                     _            _____          _ _
// |_   _| __ ___  ___| ____|_  _____  ___ _   _| |_ ___  _ _|_   _| __ __ _(_) |_
//   | || '__/ _ \/ _ \  _| \ \/ / _ \/ __| | | | __/ _ \| '__|| || '__/ _` | | __|
//   | || | |  __/  __/ |___ >  <  __/ (__| |_| | || (_) | |   | || | | (_| | | |_
//   |_||_|  \___|\___|_____/_/\_\___|\___|\__,_|\__\___/|_|   |_||_|  \__,_|_|\__|

/// Base trait for the TreeExecutor
pub trait TreeExecutorTrait
{
  /// Executor for sequential nodes
  type SeqExecutor: Executor<definitions::tst::Seq>;
  /// Executor for concurrent nodes
  type ConcExecutor: Executor<definitions::tst::Conc>;
  /// Executor for move_to nodes
  type MoveToExecutor: Executor<definitions::tst::MoveTo>;
  /// Executor for search area nodes
  type SearchAreaExecutor: Executor<definitions::tst::SearchArea>;

  /// Create a new executor for the given agent
  fn from_agent<TAgent>(agent: &TAgent) -> Self
  where
    Self::SeqExecutor: CreatableFromAgent<TAgent, Self::SeqExecutor>,
    Self::ConcExecutor: CreatableFromAgent<TAgent, Self::ConcExecutor>,
    Self::MoveToExecutor: CreatableFromAgent<TAgent, Self::MoveToExecutor>,
    Self::SearchAreaExecutor: CreatableFromAgent<TAgent, Self::SearchAreaExecutor>;
  /// Execute the tst
  fn execute(&self, t: definitions::tst::Node) -> ExecutionResult;
}

//  _____              _____                     _
// |_   _| __ ___  ___| ____|_  _____  ___ _   _| |_ ___  _ __
//   | || '__/ _ \/ _ \  _| \ \/ / _ \/ __| | | | __/ _ \| '__|
//   | || | |  __/  __/ |___ >  <  __/ (__| |_| | || (_) | |
//   |_||_|  \___|\___|_____/_/\_\___|\___|\__,_|\__\___/|_|

/// Executor of TST for an agent
pub struct TreeExecutor<
  TSeqExecutor: Executor<definitions::tst::Seq>,
  TConcExecutor: Executor<definitions::tst::Conc>,
  TMoveToExecutor: Executor<definitions::tst::MoveTo>,
  TSearchAreaExecutor: Executor<definitions::tst::SearchArea>,
> {
  seq_executor: utils::ArcMutex<TSeqExecutor>,
  conc_executor: utils::ArcMutex<TConcExecutor>,
  move_to_executor: utils::ArcMutex<TMoveToExecutor>,
  search_area_executor: utils::ArcMutex<TSearchAreaExecutor>,
}

impl<
    TSeqExecutor: Executor<definitions::tst::Seq>,
    TConcExecutor: Executor<definitions::tst::Conc>,
    TMoveToExecutor: Executor<definitions::tst::MoveTo>,
    TSearchAreaExecutor: Executor<definitions::tst::SearchArea>,
  > TreeExecutorTrait
  for TreeExecutor<TSeqExecutor, TConcExecutor, TMoveToExecutor, TSearchAreaExecutor>
{
  type SeqExecutor = TSeqExecutor;
  type ConcExecutor = TConcExecutor;
  type MoveToExecutor = TMoveToExecutor;
  type SearchAreaExecutor = TSearchAreaExecutor;
  fn from_agent<TAgent>(agent: &TAgent) -> Self
  where
    TSeqExecutor: CreatableFromAgent<TAgent, TSeqExecutor>,
    TConcExecutor: CreatableFromAgent<TAgent, TConcExecutor>,
    TMoveToExecutor: CreatableFromAgent<TAgent, TMoveToExecutor>,
    TSearchAreaExecutor: CreatableFromAgent<TAgent, TSearchAreaExecutor>,
  {
    Self {
      seq_executor: utils::arc_mutex_new(TSeqExecutor::from_agent(agent)),
      conc_executor: utils::arc_mutex_new(TConcExecutor::from_agent(agent)),
      move_to_executor: utils::arc_mutex_new(TMoveToExecutor::from_agent(agent)),
      search_area_executor: utils::arc_mutex_new(TSearchAreaExecutor::from_agent(agent)),
    }
  }
  fn execute(&self, t: definitions::tst::Node) -> ExecutionResult
  {
    dispatch_execution(self, t)
  }
}

impl<
    TSeqExecutor: Executor<definitions::tst::Seq>,
    TConcExecutor: Executor<definitions::tst::Conc>,
    TMoveToExecutor: Executor<definitions::tst::MoveTo>,
    TSearchAreaExecutor: Executor<definitions::tst::SearchArea>,
  > Executors for TreeExecutor<TSeqExecutor, TConcExecutor, TMoveToExecutor, TSearchAreaExecutor>
{
  fn execute_seq(&self, tst_node: definitions::tst::Seq) -> ExecutionResult
  {
    self.seq_executor.lock().unwrap().execute(self, tst_node)
  }
  fn execute_conc(&self, tst_node: definitions::tst::Conc) -> ExecutionResult
  {
    self.conc_executor.lock().unwrap().execute(self, tst_node)
  }
  fn execute_move_to(&self, tst_node: definitions::tst::MoveTo) -> ExecutionResult
  {
    self
      .move_to_executor
      .lock()
      .unwrap()
      .execute(self, tst_node)
  }
  fn execute_search_area(&self, tst_node: definitions::tst::SearchArea) -> ExecutionResult
  {
    self
      .search_area_executor
      .lock()
      .unwrap()
      .execute(self, tst_node)
  }
}

/// Agent executor with default for Sequence and Concurrent
pub type DefaultTreeExecutor<TMoveToExecutor, TSearchAreaExecutor> =
  TreeExecutor<SeqExecutor, ConcExecutor, TMoveToExecutor, TSearchAreaExecutor>;

#[cfg(test)]
mod tests
{
  struct FakeAgentPosition
  {
    longitude: f64,
    latitude: f64,
    altitude: f64,
  }
  struct FakeAgent
  {
    position: utils::ArcMutex<FakeAgentPosition>,
  }
  impl Agent for FakeAgent {}
  struct FakeAgentMoveToExecutor
  {
    position: utils::ArcMutex<FakeAgentPosition>,
  }
  impl CreatableFromAgent<FakeAgent, FakeAgentMoveToExecutor> for FakeAgentMoveToExecutor
  {
    fn from_agent(_agent: &FakeAgent) -> FakeAgentMoveToExecutor
    {
      FakeAgentMoveToExecutor {
        position: _agent.position.clone(),
      }
    }
  }
  impl Executor<definitions::tst::MoveTo> for FakeAgentMoveToExecutor
  {
    fn execute<'a, 'b>(
      &'a self,
      _executors: &'b impl Executors,
      t: definitions::tst::MoveTo,
    ) -> ExecutionResult<'b>
    {
      let pos = self.position.clone();
      async move {
        let mut p = pos.lock().unwrap();
        p.longitude = t.params.waypoint.longitude;
        p.latitude = t.params.waypoint.latitude;
        p.altitude = t.params.waypoint.altitude;
        Ok::<(), Error>(())
      }
      .boxed()
    }
  }
  use super::*;

  #[test]
  fn execute()
  {
    let node: definitions::tst::Node = definitions::tst::Seq::build(None)
      .add(
        definitions::tst::MoveToParameters {
          waypoint: definitions::tst::GeoPoint {
            longitude: 16.4,
            latitude: 59.3,
            altitude: 110.8,
          },
          ..Default::default()
        },
        None,
      )
      .add(
        definitions::tst::MoveToParameters {
          waypoint: definitions::tst::GeoPoint {
            longitude: 16.5,
            latitude: 59.5,
            altitude: 110.7,
          },
          ..Default::default()
        },
        None,
      )
      .into();

    let agent = FakeAgent {
      position: utils::arc_mutex_new(FakeAgentPosition {
        longitude: 0.0,
        latitude: 0.0,
        altitude: 0.0,
      }),
    };
    let executor = TreeExecutor::<
      SeqExecutor,
      ConcExecutor,
      FakeAgentMoveToExecutor,
      NoExecutor<definitions::tst::SearchArea>,
    >::from_agent(&agent);
    let r = executor.execute(node);
    let res = futures::executor::block_on(r);
    assert!(res.is_ok());
    let p = agent.position.lock().unwrap();
    assert_eq!(p.longitude, 16.5);
    assert_eq!(p.latitude, 59.5);
    assert_eq!(p.altitude, 110.7);
  }
}
