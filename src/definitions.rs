//! definitions
//! ===========
//!
//! Support for defining parts of an agent.

pub mod agent;
pub mod goal;
pub mod misc_structures;
pub mod task;
pub mod tst;
