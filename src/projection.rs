//! Handle conversion between WGS84 and a carthesian representation

/// Projection from UTM to WGS84
#[derive(Clone)]
pub struct Projection
{
  projection_utm: proj4rs::proj::Proj,
  projection_wgs84: proj4rs::proj::Proj,
  offset_x: f64,
  offset_y: f64,
}

fn utm_zone(lon: f64, lat: f64) -> i32
{
  if lat >= 56.0 && lat < 64.0 && lon >= 3.0 && lon < 12.0
  {
    return 32;
  }
  // Special zones for Svalbard
  else if lat >= 72.0 && lat < 84.0
  {
    if lon >= 0.0 && lon < 9.0
    {
      return 31;
    }
    else if lon >= 9.0 && lon < 21.0
    {
      return 33;
    }
    else if lon >= 21.0 && lon < 33.0
    {
      return 35;
    }
    else if lon >= 33.0 && lon < 42.0
    {
      return 37;
    }
  }
  return ((lon + 180.0) / 6.0) as i32 + 1;
}

impl Projection
{
  /// Create a new projection centered around (lon, lat).
  pub fn new(lon: f64, lat: f64) -> Projection
  {
    let utm_z: i32 = utm_zone(lon, lat);
    let proj_str = format!(
      "+proj=utm +zone={} +datum=WGS84 +units=m +no_defs +type=crs",
      utm_z
    );
    let proj_from = proj4rs::proj::Proj::from_proj_string(&proj_str[..]).unwrap();
    let proj_to = proj4rs::proj::Proj::from_proj_string(concat!(
      "+proj=longlat +ellps=WGS84",
      " +datum=WGS84 +no_defs"
    ))
    .unwrap();

    let mut point_3d = (lon.to_radians() as f64, lat.to_radians() as f64, 0.0);
    proj4rs::transform::transform(&proj_to, &proj_from, &mut point_3d).unwrap();

    Projection {
      projection_utm: proj_from,
      projection_wgs84: proj_to,
      offset_x: point_3d.0 as f64,
      offset_y: point_3d.1 as f64,
    }
  }
  /// Convert from (lon, lat) to (x, y)
  pub fn from_wgs84(&self, lon: f64, lat: f64) -> (f64, f64)
  {
    let mut point_3d = (lon.to_radians() as f64, lat.to_radians() as f64, 0.0);
    proj4rs::transform::transform(&self.projection_wgs84, &self.projection_utm, &mut point_3d)
      .unwrap();
    (
      point_3d.0 as f64 - self.offset_x,
      point_3d.1 as f64 - self.offset_y,
    )
  }
  /// Convert from (x, y) to (lon, lat)
  pub fn to_wgs84(&self, x: f64, y: f64) -> (f64, f64)
  {
    let mut point_3d = ((x + self.offset_x) as f64, (y + self.offset_y) as f64, 0.0);
    proj4rs::transform::transform(&self.projection_utm, &self.projection_wgs84, &mut point_3d)
      .unwrap();
    (
      point_3d.0.to_degrees() as f64,
      point_3d.1.to_degrees() as f64,
    )
  }
}
