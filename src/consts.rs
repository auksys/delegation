//! Contains constants used in agent_tk.

/// String name for TST
pub const TST: &str = "tst";
/// String name for goal
pub const GOAL: &str = "goal";

/// Length of the task queue for an agent
pub const TASK_QUEUE_LENGTH: usize = 20;
