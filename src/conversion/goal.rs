//! Support for executing goals.

use crate::{definitions, Result};

/// Convert a goal to a TST
pub struct ToTst {}

impl ToTst
{
  /// Convert a goal to a TST
  pub fn convert(goal: &definitions::goal::Goal) -> Result<definitions::tst::Node>
  {
    let mut builder = definitions::tst::Seq::build(Some(definitions::tst::CommonParameters {
      node_uuid: goal.uuid,
      ..Default::default()
    }));

    for (loc, v_goal) in goal.visit.iter()
    {
      builder = builder.add(
        definitions::tst::MoveToParameters {
          waypoint: definitions::tst::GeoPoint {
            longitude: v_goal.position.longitude,
            latitude: v_goal.position.latitude,
            ..Default::default()
          },
          ..Default::default()
        },
        None,
      );
    }
    Ok(builder.into())
  }
}
