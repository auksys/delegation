//! This module handle the execution for the agent.

use crate::{agent, delegation, module, states, utils, uuid};
pub mod default;
pub mod tst;

//  ___                   _   __  __
// |_ _|_ __  _ __  _   _| |_|  \/  | ___  ___ ___  __ _  __ _  ___
//  | || '_ \| '_ \| | | | __| |\/| |/ _ \/ __/ __|/ _` |/ _` |/ _ \
//  | || | | | |_) | |_| | |_| |  | |  __/\__ \__ \ (_| | (_| |  __/
// |___|_| |_| .__/ \__,_|\__|_|  |_|\___||___/___/\__,_|\__, |\___|
//           |_|                                         |___/

/// Input message for the execution module
#[derive(Clone)]
pub enum InputMessage
{
  /// Queue the execution of the given task
  QueueExecution
  {
    /// uuid of the task to execute
    uuid: uuid::Uuid,
  },
  /// Cancel the execution of the given task
  CancelExecution
  {
    /// uuid of the task to cancel
    uuid: uuid::Uuid,
  },
}

//   ___        _               _   __  __
//  / _ \ _   _| |_ _ __  _   _| |_|  \/  | ___  ___ ___  __ _  __ _  ___
// | | | | | | | __| '_ \| | | | __| |\/| |/ _ \/ __/ __|/ _` |/ _` |/ _ \
// | |_| | |_| | |_| |_) | |_| | |_| |  | |  __/\__ \__ \ (_| | (_| |  __/
//  \___/ \__,_|\__| .__/ \__,_|\__|_|  |_|\___||___/___/\__,_|\__, |\___|
//                 |_|                                         |___/

/// Output message for the execution module
#[derive(Clone)]
pub enum OutputMessage
{
  /// Current estimated state after the execution of the last task on the queue
  CurrentEstimatedIdlingState
  {
    /// Current estimated cost of executing the entire queue
    current_cost: f32,
    /// The current estimated states, once the ecxecution
    final_states: states::States,
    /// Number of tasks on the queue
    queue_length: usize,
  },
}

//  __  __           _       _      ___       _             __
// |  \/  | ___   __| |_   _| | ___|_ _|_ __ | |_ ___ _ __ / _| __ _  ___ ___
// | |\/| |/ _ \ / _` | | | | |/ _ \| || '_ \| __/ _ \ '__| |_ / _` |/ __/ _ \
// | |  | | (_) | (_| | |_| | |  __/| || | | | ||  __/ |  |  _| (_| | (_|  __/
// |_|  |_|\___/ \__,_|\__,_|_|\___|___|_| |_|\__\___|_|  |_|  \__,_|\___\___|

/// Module interface for the execution module
pub type ModuleInterface = module::ModuleInterface<InputMessage, OutputMessage>;

//  __  __           _       _
// |  \/  | ___   __| |_   _| | ___
// | |\/| |/ _ \ / _` | | | | |/ _ \
// | |  | | (_) | (_| | |_| | |  __/
// |_|  |_|\___/ \__,_|\__,_|_|\___|

/// Trait to define the interface to execution module
pub trait Module:
  module::Module<InputMessage = InputMessage, OutputMessage = OutputMessage> + Sized
{
  /// Creation options for the execution module
  type Options;
  /// Start an execution module
  fn start(
    agent_data: agent::AgentData,
    module_interfaces: (
      module::ModuleInterface<InputMessage, OutputMessage>,
      Self::ModulePrivateInterface,
    ),
    options: Self::Options,
  ) -> impl std::future::Future<Output = ()> + std::marker::Send + 'static;
}
