//! definition of a transport module for the delegation

use crate::{agent, delegation, module, Result};
use std::future::Future;

//  ___                   _   __  __
// |_ _|_ __  _ __  _   _| |_|  \/  | ___  ___ ___  __ _  __ _  ___
//  | || '_ \| '_ \| | | | __| |\/| |/ _ \/ __/ __|/ _` |/ _` |/ _ \
//  | || | | | |_) | |_| | |_| |  | |  __/\__ \__ \ (_| | (_| |  __/
// |___|_| |_| .__/ \__,_|\__|_|  |_|\___||___/___/\__,_|\__, |\___|
//           |_|                                         |___/

/// Input message for the transport
#[derive(Clone)]
pub enum InputMessage
{
  /// Send a cfp
  SendCFP
  {
    /// CFP
    cfp: delegation::CFP,
  },
  /// Send a proposal with the cost for executing a task
  SendProposal
  {
    /// content of the proposal
    proposal: delegation::Proposal,
  },
  /// Send a message indicating the acceptance of a proposal
  SendProposalAcceptance
  {
    /// Content of the acceptance
    acceptance: super::transport_messages::Acceptance,
  },
  /// Send a message indicating the acceptance of an execution
  SendExecutionAcceptance
  {
    /// Content of the acceptance
    acceptance: super::transport_messages::Acceptance,
  },
  /// Send the withdrawal of the acceptance
  SendCancelAcceptance
  {
    /// Content of the cancellation
    cancel_acceptance: super::transport_messages::CancelAcceptance,
  },
}

//   ___        _               _   __  __
//  / _ \ _   _| |_ _ __  _   _| |_|  \/  | ___  ___ ___  __ _  __ _  ___
// | | | | | | | __| '_ \| | | | __| |\/| |/ _ \/ __/ __|/ _` |/ _` |/ _ \
// | |_| | |_| | |_| |_) | |_| | |_| |  | |  __/\__ \__ \ (_| | (_| |  __/
//  \___/ \__,_|\__| .__/ \__,_|\__|_|  |_|\___||___/___/\__,_|\__, |\___|
//                 |_|                                         |___/

/// Output message from the transport, correspond to received messages.
#[derive(Clone)]
pub enum OutputMessage
{
  /// Contains a CFP received by the transport
  ReceivedCFP
  {
    /// CFP
    cfp: delegation::CFP,
  },
  /// Contains a proposal received by the transport
  ReceivedProposal
  {
    /// proposal
    proposal: delegation::Proposal,
  },
  /// Contains a received an accepted proposal
  ReceivedProposalAccepted
  {
    /// acceptance
    acceptance: super::transport_messages::Acceptance,
  },
  /// Contains a received an accepted execution
  ReceivedExecutionAccepted
  {
    /// acceptance
    acceptance: super::transport_messages::Acceptance,
  },
  /// Contains a cancel acceptance
  ReceivedCancelAcceptance
  {
    /// cancel the acceptance
    cancel_acceptance: super::transport_messages::CancelAcceptance,
  },
  /// Contains a received status
  ReceivedStatus
  {
    /// the status
    status: super::transport_messages::Status,
  },
}

//  __  __           _       _      ___       _             __
// |  \/  | ___   __| |_   _| | ___|_ _|_ __ | |_ ___ _ __ / _| __ _  ___ ___
// | |\/| |/ _ \ / _` | | | | |/ _ \| || '_ \| __/ _ \ '__| |_ / _` |/ __/ _ \
// | |  | | (_) | (_| | |_| | |  __/| || | | | ||  __/ |  |  _| (_| | (_|  __/
// |_|  |_|\___/ \__,_|\__,_|_|\___|___|_| |_|\__\___|_|  |_|  \__,_|\___\___|

/// Module interface for transport
pub type ModuleInterface = module::ModuleInterface<InputMessage, OutputMessage>;

//  __  __           _       _
// |  \/  | ___   __| |_   _| | ___
// | |\/| |/ _ \ / _` | | | | |/ _ \
// | |  | | (_) | (_| | |_| | |  __/
// |_|  |_|\___/ \__,_|\__,_|_|\___|

/// Trait to define the interface to transport module
pub trait Module:
  crate::module::Module<InputMessage = InputMessage, OutputMessage = OutputMessage> + Sized
{
  /// Options for the creation of the transport module
  type Options;
  /// Start a new transport module
  fn start<'a>(
    agent_data: agent::AgentData,
    module_interfaces: (
      module::ModuleInterface<InputMessage, OutputMessage>,
      Self::ModulePrivateInterface,
    ),
    delegation_module_interface: delegation::ModuleInterface,
    options: Self::Options,
  ) -> Result<futures::future::BoxFuture<'a, ()>>;
}
