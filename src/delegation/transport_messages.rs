//! Definition of messages exchanged by the transport layer.

use crate::uuid;

//  __  __
// |  \/  |   ___   ___   ___    __ _    __ _    ___   ___
// | |\/| |  / _ \ / __| / __|  / _` |  / _` |  / _ \ / __|
// | |  | | |  __/ \__ \ \__ \ | (_| | | (_| | |  __/ \__ \
// |_|  |_|  \___| |___/ |___/  \__,_|  \__, |  \___| |___/
//                                      |___/

/// Represents acceptance to execute a task.
#[derive(Clone, serde::Deserialize, serde::Serialize)]
pub struct Acceptance
{
  /// uri of this agent
  pub agent_uri: String,
  /// URI for the agent which resuest the delegation of the task.
  pub requester_uri: String,
  /// true if the agent accept the execution of the mission
  pub acceptance: bool,
  /// uuid of the CFP
  pub uuid: uuid::Uuid,
  /// signature it ingludes the agent_uri, cost, uuid and description
  pub signature: Vec<u8>,
}

/// Represents acceptance to execute a task.
#[derive(Clone, serde::Deserialize, serde::Serialize)]
pub struct CancelAcceptance
{
  /// uri of this agent
  pub agent_uri: String,
  /// uuid of the CFP/Task
  pub uuid: uuid::Uuid,
  /// signature it ingludes the agent_uri, cost, uuid and description
  pub signature: Vec<u8>,
}

/// Represents the delegation and execution status.
#[derive(Clone, serde::Deserialize, serde::Serialize)]
pub struct Status
{
  uuid: uuid::Uuid,
  status: super::Status,
}
