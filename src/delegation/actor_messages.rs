use std::time::Duration;

use super::transport_messages;

use actix::prelude::*;

//  ____           _                          _
// |  _ \    ___  | |   ___    __ _    __ _  | |_    ___
// | | | |  / _ \ | |  / _ \  / _` |  / _` | | __|  / _ \
// | |_| | |  __/ | | |  __/ | (_| | | (_| | | |_  |  __/
// |____/   \___| |_|  \___|  \__, |  \__,_|  \__|  \___|
//                            |___/

/// This message is used to delegate the given task, with an optional team name.
#[derive(Message)]
#[rtype(result = "()")]
pub struct Delegate<TTask: super::Task>
{
  pub task: TTask,
  pub team: Option<String>,
}

//  ____                       _    ____   _____   ____
// / ___|    ___   _ __     __| |  / ___| |  ___| |  _ \
// \___ \   / _ \ | '_ \   / _` | | |     | |_    | |_) |
//  ___) | |  __/ | | | | | (_| | | |___  |  _|   |  __/
// |____/   \___| |_| |_|  \__,_|  \____| |_|     |_|

#[derive(Message)]
#[rtype(result = "Vec<transport_messages::Proposal>")]
pub struct SendCFP
{
  pub cfp: transport_messages::CFP,
}

//  ____    _             _
// / ___|  | |_    __ _  | |_   _   _   ___
// \___ \  | __|  / _` | | __| | | | | / __|
//  ___) | | |_  | (_| | | |_  | |_| | \__ \
// |____/   \__|  \__,_|  \__|  \__,_| |___/

/// This message is used to send a status update
///
#[derive(Message)]
#[rtype(result = "()")]
pub struct Status
{
  pub uuid: uuid::Uuid,
  pub status: super::Status,
}

//  ____                                                 _      _                                   _
// |  _ \   _ __    ___    _ __     ___    ___    __ _  | |    / \      ___    ___    ___   _ __   | |_    __ _   _ __     ___    ___
// | |_) | | '__|  / _ \  | '_ \   / _ \  / __|  / _` | | |   / _ \    / __|  / __|  / _ \ | '_ \  | __|  / _` | | '_ \   / __|  / _ \
// |  __/  | |    | (_) | | |_) | | (_) | \__ \ | (_| | | |  / ___ \  | (__  | (__  |  __/ | |_) | | |_  | (_| | | | | | | (__  |  __/
// |_|     |_|     \___/  | .__/   \___/  |___/  \__,_| |_| /_/   \_\  \___|  \___|  \___| | .__/   \__|  \__,_| |_| |_|  \___|  \___|
//                        |_|                                                              |_|

/// This message is used to send an acceptance of a proposal
///
#[derive(Message)]
#[rtype(result = "transport_messages::Acceptance")]
pub struct SendAcceptance
{
  pub timeout: Duration,
  pub acceptance: transport_messages::Acceptance,
}

//   ____                                 _      _                                   _
//  / ___|   __ _   _ __     ___    ___  | |    / \      ___    ___    ___   _ __   | |_    __ _   _ __     ___    ___
// | |      / _` | | '_ \   / __|  / _ \ | |   / _ \    / __|  / __|  / _ \ | '_ \  | __|  / _` | | '_ \   / __|  / _ \
// | |___  | (_| | | | | | | (__  |  __/ | |  / ___ \  | (__  | (__  |  __/ | |_) | | |_  | (_| | | | | | | (__  |  __/
//  \____|  \__,_| |_| |_|  \___|  \___| |_| /_/   \_\  \___|  \___|  \___| | .__/   \__|  \__,_| |_| |_|  \___|  \___|
//                                                                          |_|

/// This message is used to send the cancelation of a proposal acceptance
///
#[derive(Message)]
#[rtype(result = "()")]
pub struct SendCancelAcceptance
{
  pub proposal: transport_messages::Proposal,
}
