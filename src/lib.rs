#![doc = include_str!("../README.MD")]
#![allow(unused)]
#![warn(missing_docs)]
#![deny(warnings)]

mod error;

pub mod agent;
pub mod consts;
pub mod conversion;
pub mod decision;
pub mod definitions;
pub mod delegation;
pub mod execution;
pub mod knowledge_base;
pub mod module;
pub mod projection;
pub mod simulation;
pub mod states;
mod statistics;
pub mod uuid;

mod utils;

pub use error::Error;
/// Result type for agent-tk
pub type Result<T> = std::result::Result<T, Error>;

pub use agent::Agent;

pub use yaaral;
