// ArcMutex

pub type ArcMutex<T> = std::sync::Arc<std::sync::Mutex<T>>;

pub(crate) fn arc_mutex_new<T>(t: T) -> ArcMutex<T>
{
  std::sync::Arc::new(std::sync::Mutex::new(t))
}

// ArcFMutex

pub(crate) type ArcFMutex<T> = std::sync::Arc<futures::lock::Mutex<T>>;

pub(crate) fn arc_fmutex_new<T>(t: T) -> ArcFMutex<T>
{
  std::sync::Arc::new(futures::lock::Mutex::new(t))
}

// ArcRwLock

pub(crate) type ArcRwLock<T> = std::sync::Arc<std::sync::RwLock<T>>;

pub(crate) fn arc_rw_lock_new<T>(t: T) -> ArcRwLock<T>
{
  std::sync::Arc::new(std::sync::RwLock::new(t))
}

// report error

pub(crate) fn log_error<T, E: std::fmt::Debug>(v: Result<T, E>)
{
  if let Err(e) = v
  {
    log::error!("An error occured: {:?}", e);
  }
}
